# -*- coding: utf-8 -*-
"""
Created on Wed Oct 04 18:00:20 2017

@author: locast
"""
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.io.shapereader as shpreader
import matplotlib.patches as mpatches
import numpy as np
import os,Tkinter,cartopy,imageio

colors = ['#FFFFFF',   # White
          (0.76147635524798152, 0.86851211072664358, 0.92456747404844286),
          (0.42068435217224132, 0.67643214148404451, 0.81868512110726643),
          (0.16339869281045752, 0.44498269896193771, 0.69750096116878124),
          (0.98246828143021925, 0.80069204152249152, 0.70611303344867393),
          (0.89457900807381785, 0.50380622837370259, 0.39976931949250294),
          (1,0,0.2),
          (0.72848904267589387, 0.15501730103806227, 0.1973856209150327),  # Red
          (1,1,0),
          (1,0.6,0), 
          (0,0.7,0),
          (0,0.3,0.2),
          (0,0.9,0.1), 
          (0.9,0.1,0.9),
          (0.545098, 0, 0),
          (0,0,0)# Black
          ]
#%%
def color_map(data,title,dico,categories = None):    
    # cartopy setup
    shpfilename = shpreader.natural_earth(resolution='110m',
                                      category='cultural',
                                      name='admin_0_countries')
    reader = shpreader.Reader(shpfilename)
    countries = reader.records()
    
    # Create categorie if not fixed
    if not categories:
        cuts = {}
        data = data.sort_values('Value')
        cut = len(data)/7
        for i in range(1,7):
            cuts[i-1]=data.Value.iloc[cut*i]
        # Legend labels
        labels = ['No Data',
                  'Value < '+str(cuts[0]),
                  'Value < '+str(cuts[1]),
                  'Value < '+str(cuts[2]),
                  'Value < '+str(cuts[3]),
                  'Value < '+str(cuts[4]),
                  'Value < '+str(cuts[5]),
                  'Value > '+str(cuts[5]),
                  'Value = 0'
                  ]
    else:
        cuts = categories['cut']
        labels = categories['label']
    # cartopy country description
    shpfilename = shpreader.natural_earth(resolution='110m',
                                      category='cultural',
                                      name='admin_0_countries')
    reader = shpreader.Reader(shpfilename)
    countries = reader.records()
    # Create plot
    fig = Figure(figsize=(8,4), dpi=100)     
    ax = fig.add_axes([0.01, 0.01, 0.98, 0.98], title =title,
                      projection=cartopy.crs.PlateCarree())
    ax.set_global()
    ax.stock_img()
    # Country coloration
    for country in countries:
        countryName = country.attributes['name_long'].decode('iso-8859-1').encode('utf8')
        try:
            countryName = dico[countryName]
        except KeyError:
            pass
        current = data[data.Area == countryName ]
        try:
            val = float(current['Value'].iloc[0])
        except KeyError:
            val = -1
        except  IndexError:
            val = -1
        
        cat = 0
        if val>0:
            for i,cut in enumerate(cuts):
                if val > cuts[cut]:
                    cat = i+1
        elif val == 0:
            cat = 6 #yellow
        else:
            cat = -1  #white 
        color = colors[cat+1]
        ax.add_geometries(country.geometry, ccrs.PlateCarree(),
                          facecolor=color,
                          label=country.attributes['adm0_a3']) 
    ax.add_feature(cartopy.feature.OCEAN)
    ax.add_feature(cartopy.feature.COASTLINE)
    ax.add_feature(cartopy.feature.BORDERS, linestyle='-', alpha=.5)
    ax.set_extent([-150, 60, -25, 60]) 
    # Figure legend
    squares = []
    for i in range(0,len(labels)):
        square = mpatches.Rectangle((0, 0), 1, 1, facecolor=colors[i])
        squares.append(square)
    fig.legend(squares, labels,'lower left')
    return fig

def showFig(fig):
    # Canevas
    root = Tkinter.Tk()
    canvas = FigureCanvasTkAgg(fig, master=root)
    canvas.show()
    canvas.get_tk_widget().pack(side=Tkinter.TOP, fill=Tkinter.BOTH, expand=1)
    toolbar = NavigationToolbar2TkAgg(canvas, root)
    toolbar.update()
    canvas._tkcanvas.pack(side=Tkinter.TOP, fill=Tkinter.BOTH, expand=1)

def toimage(fig,name):
    canvas = FigureCanvasTkAgg(fig)
    im = 'temp/{0}.png'.format(name)
    fig.savefig(im)
    image = imageio.imread(im)
    os.remove(im)
    return image

def gifCreator(images,name):   
    with imageio.get_writer('gif\{0}.gif'.format(str(name)), mode='I',duration = 1) as writer:
        for frame in images:
            writer.append_data(frame)  
  
# Bargraph
def barPlot(xlab,hight,title):

    left = range(1,len(xlab)+1)    
    fig = Figure(figsize=(8,4), dpi=100)
    
    ax = fig.add_subplot(111, title =title)
    ax.bar(left, hight,  width=0.8, bottom=None)
    convertXlab = []
    for lab in xlab:
        if type(lab) == unicode :
            lab = lab.encode('ascii','ignore')
        elif isinstance(lab,str):
            lab = lab.decode('ascii','ignore').encode('ascii')
        convertXlab.append(lab)
    plt.setp(ax, xticks=left, xticklabels= convertXlab)
    fig.autofmt_xdate(bottom=None, rotation=-90, ha='right')
    
    return fig

def curve(year,value,title,fig = None,color = None):
    if not fig :
        fig = Figure(figsize=(8,4), dpi=100)
        ax = fig.add_subplot(111, title =title)
        ax.xaxis.set_ticks(np.arange(min(year), max(year)+1, 1))
        fig.autofmt_xdate(bottom=None, rotation=-90, ha='right')
        ax.grid(True)
    else:
        ax = fig.axes[0]
    if not color:
        color = 'b'
    else:
        color = colors[color]
    ax.plot(list(year),list(value),color = color)
    
    return fig,color

def drawStacked(data,title):
    items = list(data.columns)
    items.remove('ref')
    
    fig = Figure(figsize=(8,4), dpi=100)
    ax = fig.add_subplot(111, title =title)
    left = range(1,len(data['ref'])+1)
    color = 1
    index = None
    
    ax.bar(left, data[items[0]], 1, color=colors[color])
    index = data[items[0]]
    color += 1 
    
    for item in items[1:]:
        ax.bar(left, data[item], 1, color=colors[color], bottom=index) 
        ''' sum by ref '''
        index += data[item]
        ''' '''
        color += 1
    plt.setp(ax, xticks=left, xticklabels= data['ref'])
    fig.autofmt_xdate(bottom=None, rotation=-90, ha='right')
    
    return fig