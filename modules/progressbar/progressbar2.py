# -*- coding: utf-8 -*-
import sys
def bar(progress,title,current,tabs):
    """
    This function alow the script progress visualisation,\n
    it print an output of the type: \n
    "title" [###-----] X %  "current".  with X = progress*100
    """
    barLength = 10 # Modify this to change the length of the progress bar
    margin='\r'
    margin += '\t' * tabs
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        current = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        current = "Halt...\r\n"
    if progress >= 1:
        progress = 1
    block = int(round(barLength*progress))
    text =margin+ title+": [{0}] {1}% {2}".format( "#"*block + "-"*(barLength-block),round(progress*100,2) ,'curent '+ current)
    sys.stdout.write(text)
    sys.stdout.flush()

