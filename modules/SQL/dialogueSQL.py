# -*- coding: utf-8 -*-
"""
Created on Tue Aug 01 16:38:46 2017

@author: locast
"""
import MySQLdb
import pandas as pd
#import modules.progressbar as progress
# def repertoire travail
#%%
def toColName(item):
    item = item.replace(' ','')
    item = item.replace('/','') 
    item = item.replace('-','') 
    item = item.replace('.','')  
    item = item.replace('&','')        
    item = item.replace('quantity','qtt')
    item = item.replace('commoditybalancescrops','ComBalCrop')
    item = item.replace('commoditybalanceslivestockfish','ComBalLive')
    item = item.replace('foodbalancesheets','FoodBal')
    item = item.replace('foodsupplycrops','FoodSupCrop')
    item = item.replace('foodsupplylivestockfish','FoodSupLive')
    item = item.replace('productionlivestockprimary','prodLivePrim')
    item = item.replace('productioncrops','prodCrop')
    item = item.replace('productionlivestock','prodlive')
    item = item.replace('processed','process')
    item = item.replace('(','')
    item = item.replace(')','')        
    item = item.replace('andproducts','_prods')
    item = item.replace(',','')
    item = item.replace('+','')
    item = item.replace('capita','cap')
    item = item.replace('tradecropslivestock','trades')
    item = item.replace('Excluding','_no')
    item = item.replace('Quantity','Qtt')
    item = item.replace('indigenous','Indi')
    item = item.replace('edequivalent','_eq')
    item = item.replace('Equivalent','Eq')
    item = item.replace('Forageandsilage','For_Sil')
    item = item.replace('Protein','Prot')
    item = item.replace('DryEquiv','')
    item = item.replace('yqttgcapday','y')
    
    return item
#%%
class SQLCo:
    """
    This is the SQLConexion class, this class has a set of methods listed below,
    thoose methods can operates over a MySQL database. 
    """
    def __init__(self,dbHost,dbUser,passwd):
        """
        This is the SQLCo class init function.
        """
        self.db = MySQLdb.connect(host=dbHost,
                         user=dbUser,
                         passwd=passwd,
                         local_infile = 1)
        self.cursor = self.db.cursor()
            
    def loadSql(self,filespath,files,loadCfg,columnsCfg):
        """
        Load all "files" in the database \n
        "files" is a list off the file names like "aFile.csv"\n
        "filespath" the path to the "files": "c:/..."
        "loadCfg": configuration of SQL LOAD DATA INFILE function\n
        loadCfg = CHARACTER SET utf8 
                 FIELDS TERMINATED BY ','  ENCLOSED BY '"'
                 LINES TERMINATED BY '\r\n' 
                 IGNORE 1 LINES
                 (`Area Code`, Area,`Item Code`, Item, `Element Code`, Element, `Year Code`, Year, Unit, Value,Flag) \n
        "columnsCfg" the definition of columns in "files"
        columnsCfg = (`Area Code` INT,
                     Area VARCHAR(50),
                     `Item Code` INT,
                     Item VARCHAR(500),
                     `Element Code` INT,
                     Element VARCHAR(50),
                     `Year Code` INT,
                     Year INT,
                     Unit VARCHAR(50),
                     Value VARCHAR(100),
                     Flag VARCHAR(50)
                   )
        """
        for i,csv in enumerate(files):
            path=filespath
            #print csv
            if path[-1]!='/':
                path+='/' + csv
            else:
                path += csv
            name=csv[:-4].replace('_','').replace('EAllData','').replace('(Normalized)','')
            if (name.lower(),) not in self.tables():
                self.createTable(name,columnsCfg)
                loadQuery = "LOAD DATA LOCAL INFILE '"+path+"' INTO TABLE "+name +loadCfg 
                self.cursor.execute(loadQuery )
                self.db.commit()
            else:
                print 'table '+name+' already exist'
                
            #progress.bar(float((i+1.0)/len(files)),'Loading data',name,0)
            
    def unionQuery(self,tables,selectQ,actionQ):
        """
        This function is used to create union query, alowing to select data from all "tables" \n
        selectQ is a SQL select query for exemple: "SELECT Area"\n
        actionQ is a SQL query to affine data for exemple: "WHERE Item = 'oats'" \n
        it return a str
        """
        unionQ = selectQ +" FROM `" + tables[0][0] +"` " + actionQ
        for table in tables:
            table = table[0]
            unionQ += " UNION " + selectQ + " FROM `" + table + "`" + actionQ
        return unionQ
    
    def orQuery(self,field,itemList):
        """
        This function return a orQuery with all items in "itemList" = "field"
        """

        condition ="`" + str(field) + "` = '" + str(itemList[0]) + "'"
        for item in itemList[1:]:
            condition += " or `" + str(field) + "` = '" + str(item) + "'"
        return condition
    
    def andQuery(self,field,itemList):
        """
        This function return a orQuery with all items in "itemList" = "field"
        """
        condition ="`" + field + "` = '" + itemList[0] + "'"
        for item in itemList[1:]:
            condition += " and `" + field + "` = '" + item + "'"
        return condition
    
    def dropCol(self,table,col):
        """
        This function delete the "col" column of the table "table"
        """
        sql ="alter table "+table+" drop column "+col
        self.cursor.execute(sql)
        self.db.commit()
        
    def createCol(self,table,colcfg):
        """
        This function add a column defined by "colcfg" in the table "table" \n
        colcfg is a strin like: colcfg = 'Item_categorie VARCHAR(50) NULL'
        """
        self.cursor.execute('Alter table '+table+' add '+colcfg)
        self.db.commit()
    
    def deleteQuery(self,table,condition):
        """
        This function perform a delete query on the sql table,
        condition parameter is a SQL statement like : \n
        condition = ' Where Year < 1990'
        """
        self.cursor.execute("delete from " + table + " "+condition)
        self.db.commit()
        
    def execute(self,query):
        """
        This function execute a SQL query without returning any output
        """
        self.cursor.execute(query)
        self.db.commit()
    
    def databases(self):
        """
        This function return the list of databases
        """
        self.cursor.execute('show databases')
        return self.cursor.fetchall()
    
    def tables(self):
        """
        This function return th list of tables in the database
        """
        self.cursor.execute('show tables')
        return self.cursor.fetchall()
    def columns(self,table):
        """
        This function return th list of columns in the database
        """
        columns = pd.read_sql('show columns from {0}'.format(table),self.db)
        return columns.Field
    def selectDb(self,dbName):
        """
        This function is used to select a database
        """
        self.cursor.execute('use ' +dbName)
        
    def createDb(self,dbName):
        """
        This function create a database
        """
        self.cursor.execute('create database ' + dbName)
        self.db.commit()
        
    def createTable(self,name,columns):
        """
        This function creates a SQL table.
        columns definition should be a string of type:\n
        columns = '(Area VARCHAR(50), Year INT)'
        """
        self.cursor.execute('create table ' + name + columns)
        self.db.commit()
    
    def executeReturn(self,query):
        """
        This function execute the given sql querry and return the output
        """
        self.cursor.execute(query)
        self.db.commit()
        return self.cursor.fetchall()
    
#%% Processing function   
    def cleanCollection(self,database,table, dico):
        """
        This function use the dictionary "dico" to remove data from the "table".\n
        "dico" is created via the previous function
        """
        for i,key in enumerate(dico):
            toRemove = dico[key]
            rmCondition = " WHERE "
            if isinstance(toRemove,str)  or isinstance(toRemove,int):
                rmCondition += "`"+key+"` = '" + str(toRemove)+"'"
            else:
                toRemove=list(toRemove)
                condition = self.orQuery(key,toRemove)
                rmCondition += condition
            self.deleteQuery('{0}.{1}'.format(database,table),rmCondition)
    #        progress.bar((i+1.0)/len(dico),table,'cleaning',0)
                    
    def merge(self,database,table,column, fuseName,values,action):
        """
        This function sum the values of all Area in the "countrys" list from "table" datas.
        The computed value are then inserted in "table" with "fuseName" as the Area feature
        """
        if len(self.columns(table))>8:
            sql = " insert into {0}.{1} (Area, Item, Element , Year,Unit,Value,Flag,Item_categorie)\n".format(database,table)
            sql += "select '{0}',Item, Element, Year, Unit, {1}(Value) total,'merge',Item_categorie FROM {2}.{3} where ".format(fuseName,action,database,table)
        else:
            sql = " insert into {0}.{1} (Area, Item, Element , Year,Unit,Value,Flag)\n".format(database,table)
            sql += "select '{0}',Item, Element, Year, Unit, {1}(Value) total,'add' FROM {2}.{3} where ".format(fuseName,action,database,table)
        condition = self.orQuery(column,values)
        groupColumns = ['Area','Year','Item','Element']
        groupColumns.remove(column)
        toDo = " GROUP BY {0},{1},{2}".format(groupColumns[0],groupColumns[1],groupColumns[2])
        sql += condition + toDo
        self.execute(sql)
        print self.cursor.fetchall()
        # Remove data of fusioned elements
        rmCondition =" WHERE "+condition
        self.deleteQuery('{0}.{1}'.format(database,table),rmCondition)
        # Update concat and Area_Year Values
        if len(self.columns(table))>8:
            sql = "Update {0}.{1}  set area_year = concat(Area,'_',Year), Concat = concat(Item,Unit) where {2} = '{3}'"
            sql = sql.format(database,table,column,fuseName)
            self.execute(sql)
    def categorize(self,faoDB,table, catCfgFile):
        """
        This function create a new columns named <Item categorie> in the "table", the value
        of the item categorie field is set using "catCfg".\n
        catCfg a csv categorie file :ref:`item-cat`.
        """
        # Add item cat column
        self.selectDb(faoDB)
        if 'Item_categorie' not in list(self.columns(table)):
            self.createCol(table, ' Item_categorie VARCHAR(50) NULL')
        ItemCategorieSet = pd.read_csv(catCfgFile)
        for i,catName in enumerate(ItemCategorieSet):
            catItems = ItemCategorieSet[catName]
            catItems = catItems[catItems.notnull()]
            condition = self.orQuery('Item',list(catItems))
            command = "update "+table+" set Item_categorie = '" + catName + "' where " +condition
            self.execute(command)
    #        progress.bar((i+1.0)/len(ItemCategorieSet),table,'categorise',0)
            
    def unitAjust(self,database,table,toConvert,target,factor):
        """
        This fonction can convert the "toConvert" Unit into the "target" Unit
        """
        command = "update {0}.{1} set Value = Value*{2},Flag = 'Unit', Unit = '{3}' where Unit = '{4}'".format(database,table,str(factor),target,toConvert)
        self.execute(command)
    
#    def reshapeTable(self,faoDB,database,table):
#        # Creating reshaped table
#        cols = '(area_year varchar(50) Primary key,Area varchar(50),Year varchar(50))'
#        self.createTable('{0}.{1}'.format(database,table),cols)
#        # creating index on area_year in fao table(speed the update)
#        self.execute("create index `area_year` on {0}.{1}(`area_year`)".format(faoDB, table))
#        # fill area/year value in reshaped table
#        sql = "insert into {0}.{1} (area_year,Area,Year) \n".format(database,table)
#        sql+= " SELECT area_year, Area, Year from {0}.{1} group by area_year".format(faoDB,table)
#        self.execute(sql)
#        # get list of columns to create
#        keys = pd.read_sql(" SELECT Concat,Item_categorie from {0}.{1} GROUP BY Concat".format(faoDB,table),self.db)
#        # Creating columns
#        for i,item in enumerate(keys.Concat):
#            if item != None:  #NULL
#                categorie = keys.Item_categorie.iloc[i]
#                # Adjust column name
#                name = toColName(item) 
#                if categorie == 'FAOtotal':
#                    name = categorie+'_'+name
#                self.createCol('{0}.{1}'.format(database,table),name+' FLOAT')
#                # updating column values
#                query = " SELECT area_year,Value from {0}.{1} WHERE Concat = '{2}'".format(faoDB,table,item)
#                sql = "update {0}.{1} as reshape \n".format(database,table)
#                sql += "LEFT JOIN ({0}) AS country \n".format(query)
#                sql += "ON reshape.area_year = country.area_year \n"
#                sql += "SET reshape.`{0}` =  country.Value;".format(name)
#                self.execute(sql)  
#    #            progress.bar((i+1.0)/len(keys.Concat),table,'reshape',0)
#    
#    def createLabel(self,name,labelName, LabelTable,LabelUnits,writeTable):
#        self.createCol('reshaped.'+writeTable, name+' FLOAT')
#        colname = toColName(LabelTable + labelName +LabelUnits)
#        query= """UPDATE reshaped.{0} as toupdate
#           LEFT JOIN (SELECT Area,Year,{1} from reshaped.{0} ) as datas
#           ON toupdate.Year+1=datas.Year and datas.Area = toupdate.Area
#           set toupdate.{2} = datas.{1} """.format(writeTable,colname,name) # colname instead of labelName
#        # !!! No data for Year = 2014 !!!
#        self.execute(query)
#        
#        self.createCol('reshaped.'+writeTable, name+'2 FLOAT')
#        colname = toColName(LabelTable + labelName +LabelUnits)
#        query= """UPDATE reshaped.{0} as toupdate
#           LEFT JOIN (SELECT Area,Year,{1} from reshaped.{0} ) as datas
#           ON toupdate.Year = datas.Year+1 and datas.Area = toupdate.Area
#           set toupdate.{2}2 = datas.{1} """.format(writeTable,colname,name) # colname instead of labelName
#        # !!! No data for Year = 2014 !!!
#        self.execute(query)
        
    def reshape_in_one(self,faodb,db,tables,element =None,index = True):
        import time
        start = time.time()
        
        if ('reshaped',) not in self.databases():
                self.createDb('reshaped')
        
        cols = '(area_year varchar(50) Primary key,Area varchar(50),Year varchar(50))'
        
        if element == None:
            self.createTable('{0}.{1}'.format(db,'total'),cols)
            reshapeTable = 'total'
            select1 = " SELECT area_year, Area, Year from {0}.{1} group by area_year".format(faodb,tables[0])
            select2 = " SELECT Concat,Item_categorie from {0}.{1} GROUP BY Concat"        
        elif isinstance(element,str):
            self.createTable('{0}.{1}_{2}'.format(db,'total',element),cols)
            reshapeTable = 'total'+'_'+element
            select1 = " SELECT area_year, Area, Year from {0}.{1} where Element = '{2}' group by area_year".format(faodb,tables[0],element)
            select2 = " SELECT Concat,Item_categorie from {0}.{1} where Element = '"+element+"' GROUP BY Concat"
        else:
            where = "where Element = '{0}' ".format(element[0])
            strElem = element[0]
            reshapeTable = 'total'+'_'+element[0]
            for elem in element[1:]:
                where += " or Element = '{0}' ".format(elem)
                
            select1 = " SELECT area_year, Area, Year from {0}.{1} {2} group by area_year".format(faodb,tables[0],where)
            select2 = " SELECT Concat,Item_categorie from {0}.{1} "+where+" GROUP BY Concat"
            
            self.createTable('{0}.{1}_{2}'.format(db,'total',strElem),cols)
                
        
        # fill area/year value in reshaped table
        sql = "insert into {0}.{1} (area_year,Area,Year) \n".format(db,reshapeTable)
        sql+= select1
        
        self.execute(sql)
        for table in tables:
            currtime = time.time()
            # creating index on area_year in fao table(speed the update)
            if index:
                try:
                    self.execute("create index `area_year` on {0}.{1}(`area_year`)".format(faodb, table))
                except:
                    pass
            keys = pd.read_sql(select2.format(faodb,table),self.db)
#            if table not in ['','']
#                keys = pd.read_sql(select2.format(faodb,table),self.db)
#            else:
#                keys = pd.read_sql(" SELECT Concat,Item_categorie from {0}.{1} GROUP BY Concat" .format(faodb,table),self.db)
            # Creating columns
            if len(keys) > 0 :
                for i,item in enumerate(keys.Concat):
                    if item != None:
                        categorie = keys.Item_categorie.iloc[i]
                        # Adjust column name
                        name = toColName(table+item) 
                        if categorie == 'FAOtotal':
                            name = categorie+'_'+name
                        try:
                            self.createCol('{0}.{1}'.format(db,reshapeTable),name+' FLOAT')
                        except:
                            print 'bug' + name
                        # updating column values
                        query = " SELECT area_year,Value from {0}.{1} WHERE Concat = '{2}'".format(faodb,table,item)
                        sql = "update {0}.{1} as reshape \n".format(db,reshapeTable)
                        sql += "LEFT JOIN ({0}) AS country \n".format(query)
                        sql += "ON reshape.area_year = country.area_year \n"
                        sql += "SET reshape.`{0}` =  country.Value;".format(name)
                        self.execute(sql)  
            print 'table {0}, Time {1}'.format(table,time.time()-currtime)
        print 'Time tot {0}'.format(time.time()-start)
        
        #creating label
#        self.createLabel('LABELS','Poultry Birds', 'productionlivestock','Head',reshapeTable)

                        
                        