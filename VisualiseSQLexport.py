#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 10:08:48 2017

@author: locast
"""

import pandas as pd
import Tkinter
import matplotlib.patches as mpatches
import numpy as np



import os,tkFileDialog,tkMessageBox

import modules.SQL.dialogueSQL as SQLtools
import modules.drawing.drawing as drawTools


#from PIL import Image
#import modules.progressbar.progressbar2 as progress
#%%
rootDir = os.path.abspath('.')
os.chdir(rootDir)

#%% External files
# config file
#configfile = pd.read_csv( 'configFiles/config.csv' , sep=',', header=0)
# Path for cleaning fils
cleanPath = 'configFiles/cleaningFiles/'
itemsClean = cleanPath + 'itemsClean.csv'
countrysClean = cleanPath + 'countrysClean.csv'
elementClean = cleanPath + 'ElementClean.csv'
# Path for categories files
itemCatPath = 'configFiles/categorieFiles/'
# Item categorie csv files
comBalitemCat =itemCatPath +  'itemCatComBalCrop.csv'
comBalLiveItemCat = itemCatPath + 'comBalLiveItemCat.csv'
FoodBalSheetItemCat = itemCatPath + 'FoodBalSheetItemCat.csv'
foodSupCropItemCat = itemCatPath + 'itemCatFoodSupCrop.csv'
foodSupLiveItemCat = itemCatPath + 'FoodSupLiveItemCat.csv'
inputLandItemCategorie = itemCatPath + 'InputsLandItemCat.csv'
prodCropsItemCategorie = itemCatPath + 'itemCategorieProdCrop.csv'
prodCropsProcessItemCat = itemCatPath + 'itemCategorieProdCropProcessed.csv'
prodlivestockItemCategorie = itemCatPath + 'itemCatProdLivestock.csv'
prodLivePrimItemCategorie = itemCatPath + 'itemCatProdLivePrim.csv'
TradeCropLiveItemCat = itemCatPath + 'TradeCropLiveItemCat.csv'
investmentCat =itemCatPath +'investmentCat.csv'
LiveAnCat = itemCatPath + 'LiveAnCat.csv'

#%% Dictionary
# country convertion cartopy/fao
dico = {"Bolivia" : 'Bolivia (Plurinational State of)',
"Republic of Congo" : 'Congo',
"The Gambia" : 'Gambia',
"Iran" : 'Iran (Islamic Republic of)' , 
"Lao PDR" : "Lao People's Democratic Republic" ,
"Moldova" : 'Republic of Moldova',
"Dem. Rep. Korea" : "Democratic People's Republic of Korea" ,
"Sudan" : 'Sudan (former)' , "South Sudan" : 'Sudan (former)' ,
"Syria" : 'Syrian Arab Republic' ,
"Tanzania" : 'United Republic of Tanzania' ,
"United States" : 'United States of America' ,
"Venezuela" : 'Venezuela (Bolivarian Republic of)' ,
"Vietnam" : 'Viet Nam',
"Macedonia" : 'The former Yugoslav Republic of Macedonia' 
}
# Name of boolean column in ItemCleaningcsv/and elemClean.csv  
itemCleanDict = {'commoditybalancescrops': 'keep1' ,
               'commoditybalanceslivestockfish': 'keep2',
               'foodbalancesheets': 'keep3',
               'foodsupplycrops': 'keep4',
               'foodsupplylivestockfish': 'keep5',
               'inputsland': 'keep6',
               'productioncrops': 'keep7',
               'productioncropsprocessed': 'keep8',
               'productionlivestock': 'keep9',
               'productionlivestockprimary': 'keep10',
               'tradecropslivestock': 'keep11'}
#itemCleanDict = {x:x+'_keep' for x in  
# witch file to use for categiries each collection
itemCatDict = {'commoditybalancescrops': comBalitemCat ,
               'commoditybalanceslivestockfish': comBalLiveItemCat,
               'foodbalancesheets': FoodBalSheetItemCat,
               'foodsupplycrops': foodSupCropItemCat,
               'foodsupplylivestockfish': foodSupLiveItemCat,
               'inputsland': inputLandItemCategorie,
               'productioncrops': prodCropsItemCategorie,
               'productioncropsprocessed': prodCropsProcessItemCat,
               'productionlivestock': prodlivestockItemCategorie,
               'productionlivestockprimary': prodLivePrimItemCategorie,
               'tradecropslivestock': TradeCropLiveItemCat,
                'investmentcreditagriculture': investmentCat,
                'tradeliveanimals':LiveAnCat}

# Continent in data
continent = ['Central America','Central Asia','Eastern Africa',
      'Eastern Asia','Eastern Europe','Middle Africa',
      'Northern Africa','Northern America','Northern Europe',
      'South Africa','South America','South-Eastern Asia',
      'Southern Africa','Southern Asia','Southern Europe',
      'Western Africa','Western Asia','Western Europe',
      'European Union (exc intra-trade)']

'''
# Colors Map/Legend
colors = ['#FFFFFF',   # White
              (0 , 0.80, 0.81),
              (0.28 , 0.23, 0.54),
              (0.13, 0.5, 0.13),
              (1, 0.83, 0),
              (0.70, 0.13, 0.13),
              (0.545098, 0, 0),  # Red
              (0, 0, 0)    # Black
              ]
'''
#%%
def exportUniqueCountry(save = False):
    """
    export a list of country wich are not on all the tables,
    as all the tables have a different set of country and some like reunion are only present one one table
    wich furtheremore is the lightest
    """
    ########### get a list of country
    tablesCountrys = pd.DataFrame([])
    countrys = pd.DataFrame([])
    unique = pd.DataFrame([])
    final = []
    for table in SQL.tables():
        table = table[0]
        query = 'select Area from {0} group by Area'.format(table)
        tablesCountrys[table] = pd.read_sql(query,SQL.db)
        countrys = pd.concat([countrys,tablesCountrys[table]])
    countrys = countrys.rename(columns = {0:'Area'})
    unique['Area']=countrys.Area.unique()
    del countrys
    ########## check wich country are on wich tables
    ### remove country that are on each tables
    for area in unique.Area:
        count = 0 #count how many time value has been found if 11 its in each tables
        intables = [] # name of tables wich contain the value
        notin = []  # name of tables wich not contain the value
        for table in tablesCountrys:
            notfound = True
            for tableArea in tablesCountrys[table]:
                if tableArea == area:
                    count +=1
                    intables.append(table)
                    notfound = False
            if notfound:
                notin.append(table)
        if count < len(SQL.tables()):
            final.append([area,count,intables,notin])
    
    final = pd.DataFrame(final,columns = ['Area','count','tables','noTables'])
    if save:
        final.to_csv('Country check tables.csv')
    else:
        return final

def ElemCleaner():
    to_clean = pd.read_csv(elementClean)
    for table in SQL.tables():
        table = table[0]
        tempClean = to_clean[[table,itemCleanDict[table]]]
        if len(tempClean)>0:
            tempClean = tempClean[tempClean[itemCleanDict[table]] == 0]
            for element in tempClean[table]:
                SQL.deleteQuery(table,' where Element = "{0}"'.format(element))
    
    
def learningReshape(limitCount):
    final = exportUniqueCountry()
    countrylist = []
    for i,area in enumerate(final['Area']):
        if final['count'].iloc[i]<limitCount: #limitcount = 6 
            countrylist.append(area)
    return countrylist
#%%
def cleanDictCreator(configFileDict,tables):
    """
    This function create the configuration dictionary used to clean the database
    the output is a dictionary specifying what will be removed in each tables using the
    Cleaning csv :ref:`data-clean`
    """
    cleanDict = {'ALL' : {}}
    for key in configFileDict:
        if configFileDict[key]['type'] == 'file':
            data = pd.read_csv(configFileDict[key]['file'])
            if configFileDict[key]['config'] == 'ALL':
                data = data[data['keep'] == 0]
                data.drop('keep',axis=1,inplace=True)
                cleanDict['ALL'][key]=data[data.columns[0]]
            else:
                for table in configFileDict[key]['config']:
                    tempData = data[[table,configFileDict[key]['config'][table]]]
                    tempData = tempData[tempData[configFileDict[key]['config'][table]] == 0]
                    tempData.drop(configFileDict[key]['config'][table],axis=1,inplace=True)
                    cleanDict[table] ={key : tempData[tempData.columns[0]]}
        elif configFileDict[key]['type'] == 'Value':
            val = configFileDict[key]['Value']
            if configFileDict[key]['config'] == 'ALL':
                cleanDict['ALL'][key]=val
            else:
                pass
    return cleanDict


#%% GUI faoTools
class WorldMapWindow(Tkinter.Tk):
#    def testbar(self):
#        import time
#        progress = ProgressBar(self,100)
#        for i in range(0,100):
#            progress.update(i+1)
#            time.sleep(1)
            
    def __init__(self,parent):
        Tkinter.Tk.__init__(self,parent)
        self.parent = parent

        self.selected={'table':[] , 'Year':[], 'Item':[],'Element':[], 
                       'Unit':[], 'Area':[], 'Item_categorie':[],'columns':[],'categorieFile':[] }
        
        self.objects = {'Area':[] ,'Year':[], 'Item':[],'Element':[],
                        'Unit':[], 'Item_categorie':[],'columns':[] }
        self.labels={}
        self.lists={}
        self.widgs = {}
        self.infoLabel=[]
        self.button=[]
        
        # Window menu
        menu = Tkinter.Menu(self)
        # datbase menu
        dataMenu = Tkinter.Menu(menu, tearoff=0)
        dataMenu.add_command(label="Load Data", command=lambda : switch('loading'))
        dataMenu.add_command(label="Categorize item", command=lambda : switch('categorise'))
        dataMenu.add_command(label="Remove data", command=lambda : switch('remove'))
        dataMenu.add_command(label="Merge data", command=lambda : switch('merge'))
        dataMenu.add_command(label="Run SQL query", command=lambda : switch('SQL'))
        dataMenu.add_command(label="reshape data", command=lambda : switch('reshape'))
        dataMenu.add_separator()
        dataMenu.add_command(label="Full processing", command=lambda : switch('fullProcess'))
        menu.add_cascade(label="Data tools", menu=dataMenu)
        # visualisation menu
        drawMenu = Tkinter.Menu(menu, tearoff=0)
        drawMenu.add_command(label="Map", command=lambda : switch('MAP'))
        drawMenu.add_command(label="BarGraph", command=lambda : switch('BAR'))
        drawMenu.add_command(label="Time series", command=lambda : switch('curves'))
        menu.add_cascade(label="Visualization", menu=drawMenu)
        # Export menu
        exportMenu = Tkinter.Menu(menu, tearoff=0)
        exportMenu.add_command(label="Export database", command=lambda : switch('Export'))
        exportMenu.add_command(label="Export Distinct object", command=lambda : switch('distinct'))
        menu.add_cascade(label="Export data", menu=exportMenu)
        # Learning menu
#        learningMenu = Tkinter.Menu(menu, tearoff=0)
#        learningMenu.add_command(label="clustering", command=lambda : switch('Cluster'))
#        learningMenu.add_command(label="", command=lambda : switch(''))
#        menu.add_cascade(label="Learning", menu=learningMenu)
        self.config(menu=menu)
            
        def switch(window):     
#            self.window.destroy()
            for feature in self.lists:
                self.lists[feature].destroy()
            for feature in self.labels:
                self.labels[feature].destroy()
            for label in self.infoLabel:
                label.destroy()
            for button in self.button:
                button.destroy()
            for widg in self.widgs:
                self.widgs[widg].destroy()
            self.initialize(window)
        
        self.initialize('loading')
    
    def error(self,text):
        tkMessageBox.showinfo("Error",text)
        
#%% visualisation function
    def drawMap(self): # called on draw clik
        Element = self.selected['Element'][0]
        Year = self.selected['Year'][0]
        table = self.selected['table'][0]
        Item_Categorie = self.selected['Item_categorie'][0]
        Item = self.selected['Item'][0]
        title ='Value of: {0}, , in: {1},\n Element = {2}, Unit = {3}, ponderation = {4}'
        if  not Year or not Item or not Element or not table or not Item_Categorie:
#            print 'select a collection,a Year and an item/elem first'
            self.error('not enouth data selected')
        else:
            ponderate = self.ponderate.get()
            if Year == 'ALL':
                frames = []
                query = "select * from {0} where Item = '{1}' and Element = '{2}'"
                query = query.format(table,Item,Element)
                data = pd.read_sql(query, SQL.db)
                #replace blanks with 0 
                data = data.replace('',0)
                data = data.replace('NULL',0)
                data = data.replace(np.nan,0)
                # remove all blanks 
                data = data[data.Value > 0 ] 
                # data ponderation
                if ponderate == 0:
                    ponderate = 'None'
                if ponderate == 1:
                    popData = pd.read_sql("Select * from foodbalancesheets where Item = 'Population'",SQL.db)
                    popData = popData[['Area','Year','Value']]
                    popData.columns = ['Area','Year','pop']
                    popData['pop'] = pd.to_numeric(popData['pop'])
                    data =data.merge(popData, how='left', on = ['Area','Year'])
                    data['Value']=data['Value']/data['pop']
                    ponderate = 'population'
                elif ponderate == 2:
                    ponderate = 'Agricultural area'
                    surfaceData = pd.read_sql("select Area,Year,Value from inputsland where Item = 'Agricultural area' and Element = 'Area' ",SQL.db)
                    surfaceData.columns=['Area','Year','surface']
                    surfaceData.replace(np.nan,-1,inplace=True)
                    surfaceData.replace('',-1,inplace=True)
                    surfaceData.replace(0,-1,inplace=True)
                    data = data.merge(surfaceData,how='left',on=['Area','Year'])
                    data['Value'] = data['Value']/data['surface']
                    data=data[data.Value>0]
                    
#                temp = data[data.Year== np.max(data.Year)]
                temp =pd.to_numeric(data['Value'])
                temp = temp.sort_values()
                # Cut data
                cuts={}
                cut = len(temp)/7
                for i in range(1,7):
                    cuts[i-1]=temp.iloc[cut*i]
                    print temp.iloc[cut*i]
#                    cuts[i-1]=temp.Value.iloc[cut*i]
#                    print data.Value.iloc[cut*i]
                # Cut labels
                labels = ['No Data',
                      'Value < '+str(cuts[0]),
                      'Value < '+str(cuts[1]),
                      'Value < '+str(cuts[2]),
                      'Value < '+str(cuts[3]),
                      'Value < '+str(cuts[4]),
                      'Value < '+str(cuts[5]),
                      'Value > '+str(cuts[5]),
                      'Value = 0'
                      ]

                sep = {'cut':cuts,'label':labels}
                for i,year in enumerate(self.objects['Year'].Year):
                    curent = data[data.Year == year]
                    curenttitle = title.format(Item,str(year),Element,data.Unit[0],ponderate)
                    fig = drawTools.color_map(curent, curenttitle, dico, categories = sep)
#                    showFig(fig)
                    frames.append(drawTools.toimage( fig,i ) )
#                    drawTools.toimage( fig,i )   range(len(self.objects['Year'].Year))
                drawTools.gifCreator(frames,'{0}{1}{2}{3}'.format(Item,Element,data.Unit[0],ponderate) )
                
            else:
                # Get data
                query = "select * from {0} where Year = {1} AND Item = '{2}' and Element = '{3}'"
                query = query.format(table,Year ,Item,Element)
                data = pd.read_sql(query, SQL.db)
                #replace blanks with 0 
                data = data.replace('',0)
                data = data.replace('NULL',0)
                data = data.replace(np.nan,0)
                data['Value'] = pd.to_numeric(data['Value'])
                # remove all blanks 
                data = data[data.Value > 0 ] 
                # data ponderation
                if ponderate == 0:
                    ponderate = 'None'
                if ponderate == 1:
                    popData = pd.read_sql("Select * from foodbalancesheets where Item = 'Population' and Year = "+Year,SQL.db)
                    popData = popData[['Area','Value']]
                    popData.columns = ['Area','pop']
                    popData['pop'] = pd.to_numeric(popData['pop'])
                    data =data.merge(popData, how='left', on = 'Area')
                    data['value']=data['Value']/data['pop']
                    ponderate = 'population'
                elif ponderate == 2:
                    ponderate = ''
                title = title.format(Item,str(Year),Element,data.Unit[0],ponderate)
                fig = drawTools.color_map(data, title, dico)
                drawTools.showFig(fig)

    def drawCurves(self):
        
        "year = all + 1 feature a all "
        features = ['Area','Element','Item_categorie','Item']
        if self.selected['Item']=='SUM':
            query = "select *,SUM(Value) SumVal from {0} where ".format(self.selected['table'][0])
            group = ' GROUP BY Item_Categorie,Year'
            Column = 'SumVal'
        elif self.selected['Item']=='MEAN':
            query = "select *, AVG(Value) MeanVal from {0} where ".format(self.selected['table'][0])
            group = ' GROUP BY Item_Categorie,Year'
            Column = 'MeanVal'
        else:
            query = "select * from {0} where ".format(self.selected['table'][0])
            group = ''
            Column = 'Value'
        count = 0
        draw = None
        for val in features:
            if self.selected[val][0] == 'ALL' or len(self.selected[val]) > 1:
                 count+=1
                 draw = val
            elif self.selected[val][0]=='SUM' or self.selected[val][0] == 'MEAN':
                pass
            else:
                 query+= " {0} = '{1}' and ".format(val,self.selected[val][0])
        if count <= 1:
#            years = pd.read_sql("select Year from {0} group by Year".format(self.selected['table'][0]),SQL.db)
            query = query[:-4]
            
            # get data
            data = pd.read_sql(query+group,SQL.db)
            data = data.replace('',0)
            data = data.replace('NULL',0)
            print data
            years = pd.DataFrame(data.Year.unique(),columns=['Year'])
            #title
            title = "Area : {0} , Year:ALL, Element:{1}, \n Item_categorie : {2} , Item:{3}, Unit:{4}"
            title = title.format(self.selected['Area'][0],self.selected['Element'][0],
                                 self.selected['Item_categorie'][0], self.selected['Item'][0], str(data.Unit.unique()))
            if count == 1:
                fig = None
                color = 1
                labels = []
                squares = []
#                print self.selected[draw][0]
                if self.selected[draw][0] == 'ALL':
                    for i, item in enumerate(self.objects[draw][draw]):
                        if item != 'ALL':
                            temp = data[data[draw]==item]
                            print temp
                            years = years.merge(temp[['Year',Column]],on = 'Year')
                            years.replace(np.nan,0,inplace=True)
                            print years['Year']
                            fig,legColor = drawTools.curve(years['Year'],years[Column],title,fig=fig,color = i+1)
                            years = years.rename(columns = {Column:SQLtools.toColName(item)})
                            # updating legend
                            square = mpatches.Rectangle((0, 0), 1, 1, facecolor=legColor)
                            labels.append(SQLtools.toColName(item))
                            squares.append(square)
                            color+=1
                else:
                    for i, item in enumerate(self.selected[draw]):
                        temp = data[data[draw]==item]
                        years = years.merge(temp[['Year',Column]],on = 'Year')
                        years.replace(np.nan,0,inplace=True)
                        fig,legColor = drawTools.curve(years['Year'],years[Column],title,fig=fig,color = i+1)
                        years = years.rename(columns = {Column:SQLtools.toColName(item)})
                        # updating legend
                        square = mpatches.Rectangle((0, 0), 1, 1, facecolor=legColor)
                        labels.append(SQLtools.toColName(item))
                        squares.append(square)
                        color+=1
                fig.legend(squares, labels,'upper left')
                drawTools.showFig(fig)
    
            else:
                years = years.merge(data[['Year','Value']],on = 'Year')
                years.replace(np.nan,0,inplace=True)
                fig,col = drawTools.curve(years['Year'], years['Value'],title)
                drawTools.showFig(fig)
        else:
            self.error('You must chose only ons list')
#            print 'only one all'
    
    def drawBar(self): # called on draw clik
         Year = self.selected['Year'][0]
         Element = self.selected['Element'][0]
         Item = self.selected['Item'][0]
         table = self.selected['table'][0]
         Area = self.selected['Area'][0]
         Item_Categorie = self.selected['Item_categorie'][0]

         if  not Year or not Item or not Element or not table or not Item_Categorie:
#            print 'select a collection,a Year and an item/elem first'
            self.error('not enouth data selected')
         else:
                 
             values = ['Area','Year','Element','Item_categorie','Item']
             if Item == 'MEAN':
                 query = "select *,AVG(Value) MeanVal from {0} where ".format(table)
                 group = ' GROUP BY Item_Categorie'
                 Column = 'MeanVal'
             elif Item == 'SUM':
                 query = "select *,SUM(Value) SumVal from {0} where ".format(table)
                 group = ' GROUP BY Item_Categorie'
                 Column = 'SumVal'  
             else:
                 query = "select * from {0} where ".format(table)
                 group = ''
                 Column = 'Value'
             
             draw=[]
             count =0
             for val in values:
                 if self.selected[val][0] == 'ALL':
                     count+=1
                     draw.append(val)
                 elif self.selected[val][0]=='SUM' or self.selected[val][0]=='MEAN':
                     pass
                 else:
                     query+= " ({0} = '{1}') and ".format(val,self.selected[val][0])
                     
             query = query[:-4]    
             # Load data
             data = pd.read_sql(query+group,SQL.db)
             data = data.replace('',0)
             data = data.replace('NULL',0)
             #title
             title = "Area : {0} , Year:{1}, Element:{2}, \n Item_categorie : {3} , Item:{4}, Units:{5}"
             title = title.format(Area,Year,Element,Item_Categorie,Item,data.Unit.unique())
             if count < 1:
                 self.error( ' Need to select at least 1 ALL ')
             elif count == 1:
                 drawTools.showFig(drawTools.barPlot(data[draw[0]] , data[Column],title))
                 
#             elif count == 2:
#                 item1 = pd.read_sql('select {0} from {1} group by {0}'.format(draw[0], table),SQL.db)
#                 item2 = pd.read_sql('select {0} from {1} group by {0}'.format(draw[1], table),SQL.db)
#                 data = data[[Column,draw[0],draw[1]]]
#                 if len(item1)>len(item2):
#                    ref = item1
#                    data = data.rename(columns = {draw[0] : 'ref'})
#                    stack = item2
#                    stackName = draw[1]
#                 else:
#                    ref = item2
#                    data = data.rename(columns = {draw[1] : 'ref'})
#                    stack = item1
#                    stackName = draw[0]
#                 print stackName
#                 print '_________________________________'
#                 stackdata = pd.DataFrame(ref,columns = ['ref'])
#                 for item in stack:
#                     temp = data[data[stackName] == str(item)]
#                     stackdata = stackdata.merge(temp, on = 'ref')
#                     stackdata.drop(stackName,axis = 1,inplace = True)
#                     stackdata = stackdata.rename(columns = {Column : item})
#                     
#                 drawTools.showFig(drawTools.drawStacked(stackdata ,title))         
#%% Database function
    def dataLoad(self):
        csvFiles = configfile.toLoad 
        # Create db if it dosen't exist
        if (dbName,) not in SQL.databases():
            print 'creating database'
            SQL.createDb(dbName)
        SQL.selectDb(dbName)
        # Load needed files
        if len(SQL.tables()) >= len(csvFiles):
            self.error('Non empty db selected, skipping data loading')
#            print 'Non empty db selected, skipping data loading'
        else:
            #load the data
            print 'loading data'
            loadCfg = """ CHARACTER SET latin1
                     FIELDS TERMINATED BY ','  ENCLOSED BY '"'
                     LINES TERMINATED BY '\r\n' 
                     IGNORE 1 LINES
                     (`Area Code`, Area,`Item Code`, Item, `Element Code`, Element, `Year Code`, Year, Unit, Value,Flag) 
                  """
            columnsCfg = """
                       (`Area Code` INT,
                         Area VARCHAR(50),
                         `Item Code` INT,
                         Item VARCHAR(500),
                         `Element Code` INT,
                         Element VARCHAR(50),
                         `Year Code` INT,
                         Year INT,
                         Unit VARCHAR(50),
                         Value VARCHAR(50),
                         Flag VARCHAR(50)
                       )
                        """
            SQL.loadSql(configfile.toLoadFilePath[0],csvFiles,loadCfg,columnsCfg)
         
    def fullProcess(self):
        SQL.selectDb(dbName)
        tables = SQL.tables()
        cleanDict = cleanDictCreator({'Area' : {'file':countrysClean, 'config': 'ALL','type':'file'},
                                  'Item' : {'file':itemsClean, 'type':'file','config' : itemCleanDict} }, tables)
        ElemCleaner()
        # Removing data in 'hg/ha' in productionCrops
        SQL.deleteQuery(" productioncrops "," WHERE Unit = 'hg/ha'")
        # Removing data in US$ from tradecropslivestock
#        SQL.deleteQuery(" tradecropslivestock "," WHERE Unit = '1000 US$'")
#        SQL.deleteQuery(" tradecropslivestock "," WHERE Unit = '1000US$'")
        # Removing "Other uses" Element
        SQL.deleteQuery('foodbalancesheets'," WHERE Element = 'Other uses' ")
        SQL.deleteQuery('commoditybalanceslivestockfish'," WHERE Element = 'Other uses' ")
        SQL.deleteQuery('commoditybalancescrops'," WHERE Element = 'Other uses' ")
#        progress = ProgressBar(self,len(tables))        
        for table in tables:
            table = table[0]
            # Removing Australia & New Zealand as we have data for both
            SQL.deleteQuery(table,"where Area = 'Australia & New Zealand'")
            # Removing old data/some items and countrys 
            SQL.deleteQuery(table, " WHERE Year < "+str(limiteYear))
            # remove all blanks
            SQL.deleteQuery(table, " WHERE Value = '' ")
            SQL.cleanCollection(dbName,table,dict(cleanDict[table],**cleanDict['ALL']))
            
            # Remove columns
#            SQL.dropCol(table,'`Area Code`')
#            SQL.dropCol(table,'`Year Code`')
#            SQL.dropCol(table,'`Item Code`')
#            SQL.dropCol(table,'`Element Code`')

#            #%% Fusing the countrys
#            countrysList = [['Belgium','Luxembourg'],
#                        ['Slovakia', 'Czechia'],
#                        ['South Sudan', 'Sudan'],
#                        ['Ethiopia', 'Eritrea'],
#                        ['Montenegro', 'Serbia'],
#                        ['The former Yugoslav Republic of Macedonia',
#                             'Croatia', 'Slovenia', 'Bosnia and Herzegovina',
#                             'Serbia and Montenegro']]#,
##                        ['Russian Federation', 'Kazakhstan', 'Armenia',
##                             'Tajikistan', 'Turkmenistan', 'Uzbekistan',
##                             'Ukraine', 'Estonia', 'Lithuania', 'Latvia',
##                             'Kyrgyzstan', 'Georgia', 'Azerbaijan', 
##                             'Belarus', 'Republic of Moldova']]
#            names = ['Belgium-Luxembourg',
#                'Czechoslovakia',
#                'Sudan (former)',
#                'Ethiopia PDR',
#                'Serbia and Montenegro',
#                'Yugoslav SFR']#,
##                'USSR']
#            for i,name in enumerate(names):
#                SQL.merge(dbName,table,'Area',name,countrysList[i],'SUM')
##                progress.bar((i+1.0)/len(names),table,'fusing country',0)


            # creating item cat
            try:
                SQL.categorize(dbName,table, itemCatDict[table])
            except:
                self.error('no categorie file for table'+table)
            # add area_year column
            SQL.createCol(table,"area_year VARCHAR(50) ")
            SQL.execute('Update '+table+" set area_year =concat(Area,'_',Year)")
            
            #Adjusting units
            SQL.unitAjust(dbName,table, '1000 Head' , 'Head', 1000 )
            SQL.unitAjust(dbName,table, '1000 ha' , 'ha', 1000 )
            SQL.unitAjust(dbName,table, 'kg' , 'tonnes', 0.001 )
            SQL.unitAjust(dbName,table, '1000 tonnes' , 'tonnes', 1000 )
            SQL.unitAjust(dbName,table, 'million tonnes' , 'tonnes', 1000000 )
            SQL.unitAjust(dbName,table, 'hg/An' , '0.1g/An', 1000 ) 
            SQL.unitAjust(dbName,table, '100mg/An' , '0.1g/An', 1 )
            SQL.unitAjust(dbName,table, '1000 No' , 'No', 1000 )
            
#            if table == 'productionlivestock':
#                SQL.unitAjust(dbName,table, '1000 Head' , 'Head', 1000 )
#            elif table == 'inputsland':
#                SQL.unitAjust(dbName,table, '1000 ha' , 'ha', 1000 )
#                SQL.unitAjust(dbName,table, 'million tonnes' , 'tonnes', 1000000 )
#            elif table == 'foodbalancesheets':
#                SQL.unitAjust(dbName,table, '1000 tonnes' , 'tonnes', 1000 )
#                SQL.unitAjust(dbName,table, 'kg' , 'tonnes', 0.001 )    
#            elif table == 'productionlivestockprimary':
#                SQL.unitAjust(dbName,table, '1000 Head' , 'Head', 1000 )
#                SQL.unitAjust(dbName,table, '1000 No' , 'No', 1000 )
#                SQL.unitAjust(dbName,table, '100mg/An' , '0.1g/An', 1 )
#                SQL.unitAjust(dbName,table, 'hg/An' , '0.1g/An', 1000 )    
#            elif table == 'foodsupplylivestockfish':
#                SQL.unitAjust(dbName,table, '1000 tonnes' , 'tonnes', 1000 )
#                SQL.unitAjust(dbName,table, 'kg' , 'tonnes', 0.001 )    
#            elif table == 'foodsupplycrops':
#                SQL.unitAjust(dbName,table, 'kg' , 'tonnes', 0.001 )
            
#            elif table == 'tradeliveanimals':
#                SQL.unitAjust(dbName,table, '1000 Head' , 'Head', 1000 )
                
                
            # Add concat column
            SQL.createCol(table,'Concat VARCHAR(100)')
            SQL.execute('Update '+table+" set Concat = Concat(Element,Item,Unit)")# where Item_categorie != 'FAO total'")
            
        """ Reshaping """
        #Creating reshaped databases
        if ('reshaped',) not in SQL.databases():
            SQL.createDb('reshaped')
        ''' option pour choisir'''
#            SQL.reshapeTable(dbName,'reshaped',table)
        # try reshape + merge
        tables = [x[0] for x in SQL.tables()]
        # normal reshape
        SQL.reshape_in_one(dbName,'reshaped',tables)
#        SQL.reshape_in_one(dbName,'reshaped',tables,element = ['Feed','Stocks','Laying','Milk Animals','Production','Yield','Yield/Carcass Weight','Export Quantity','Import Quantity'],index = False)
        #Add Labels
#        SQL.createCol('reshaped.productionlivestock', 'LABELS FLOAT')
#        query= """UPDATE reshaped.productionlivestock as toupdate
#           LEFT JOIN (SELECT Year,`PoultryBirdsHead` from reshaped.productionlivestock ) as datas
#           ON toupdate.Year+1=datas.Year
#           set toupdate.LABELS = datas.PoultryBirdsHead """

#        SQL.createCol('reshaped.total', 'LABELS FLOAT')
#        colname = SQLtools.toColName('productionlivestock' + 'PoultryBirdsHead')
#        query= """UPDATE reshaped.total as toupdate
#           LEFT JOIN (SELECT Year,`{0}` from reshaped.total ) as datas
#           ON toupdate.Year+1=datas.Year
#           set toupdate.LABELS = datas.{0} """.format(colname)
#        # !!! No data for Year = 2014 !!!
#        SQL.execute(query)
    def reshapedb(self):
        if ('reshaped',)in SQL.databases():
            SQL.execute('drop database reshaped')
        SQL.selectDb(dbName)
        tables = [x[0] for x in SQL.tables()]
        SQL.createDb('reshaped')
        SQL.reshape_in_one(dbName,'reshaped',tables)
        
    def exportDistinct(self):
        pass
    
    def runQuery(self):
        query = self.lists['query'].get("1.0",'end')
        result = SQL.executeReturn(query)
        self.lists['return'].delete(1.0, 'end')
        self.lists['return'].insert('end',str(result))
    
    def export(self):
        """  to select 
        Label must always be selected
        """
#        print self.selected['table']
        limit = 9
        """  to select """
        if not self.selected['table']:
            self.error( 'select a table')
        else:
            # select the countrys
            if self.area.get() == 'ALL':
                where = ''
            elif self.area.get() == 'continent':
                where = ' where ' 
                where += SQL.orQuery('Area',continent)
            elif self.area.get() == 'country':
                where = ' where '
                where += SQL.andQuery('Area',continent)
                where = where.replace('=','!=')
            # connect to the right db
            if self.clean.get() == 'learning':
                toremove = learningReshape(limit)
                if where == '':
                    where += ' where ' + SQL.andQuery('Area',toremove).replace('=','!=')
                else:
                    where += ' and ' + SQL.andQuery('Area',toremove).replace('=','!=')
            if self.db.get() == 'reshaped':
                db='reshaped'
                select = 'Area,Year,area_year'
            else:
                db = dbName
                select = '*'
#            SQL.selectDb(db)
            # Export data    
            if self.selected['table'][0] == 'ALL':
                for table in SQL.tables():
                    table=table[0]
                    columns = pd.read_sql("show columns from {0}.{1}".format(db,table), SQL.db)
                    columns = list(columns['Field'])
                    columns.remove('Area')
                    columns.remove('Year')
                    if self.db.get() == 'reshaped':
                        if self.item.get() == 'ALL':
                                select = '*'
                        else:
                            select = 'Area,Year,area_year'
                            for col in columns:
#                                print col.split('_')[0]
                                if self.item.get() == 'total':
                                    if col.split('_')[0] == 'FAOtotal':
                                        select += ',{0}'.format(col)
                                else:
                                    if col.split('_')[0] != 'FAOtotal':
                                        select += ',{0}'.format(col)
                    else:
                        if self.item.get() == 'total':
                            query = "Item_categorie = 'FAOtotal'"
                        else:
                            query = "Item_categorie != 'FAOtotal'"
                        if where == '':
                            where += ' where '+query
                        else:
                            where +=' and '+ query
                    data = pd.read_sql("select {0} from {1}.{2} {3}".format(select,db,table,where), SQL.db)
                    data.to_csv('{0}\\{1}_{2}_{3}_{4}.csv'.format(self.selected['saveDir'],db,table,self.area.get(),self.item.get())) 
            else:
                columns = pd.read_sql("show columns from {0}.{1}".format(db,self.selected['table'][0]), SQL.db)
                columns = list(columns['Field'])
                columns.remove('Area')
                columns.remove('Year')
                if self.db.get() == 'reshaped':
                    if self.item.get() == 'ALL':
                            select = '*'
                    else:
                        for col in columns:
#                            print col.split('_')[0]
                            if self.item.get() == 'total':
                                if col.split('_')[0] == 'FAOtotal':
                                    select += ',{0}'.format(col)
                            else:
                                if col.split('_')[0] != 'FAOtotal':
                                    select += ',{0}'.format(col)
                else:
                    if self.item.get() == 'total':
                        query = "Item_categorie = 'FAOtotal'"
                    else:
                        query = "Item_categorie != 'FAOtotal'"
                    if where == '':
                        where += ' where '+query
                    else:
                        where +=' and '+ query
                data = pd.read_sql("select {0} from {1}.{2} {3}".format(select,db,self.selected['table'][0],where), SQL.db)
                data.to_csv('{0}\\{1}_{2}_{3}_{4}.csv'.format(self.selected['saveDir'],db,self.selected['table'][0],self.area.get(),self.item.get())) 
                
    def fileSelection(self):
        self.selected['categorieFile'] = tkFileDialog.askopenfilename(initialdir = "/", title = "Select categorie file",
                                                                 filetypes = (("csv files","*.csv"),("all files","*.*")))
        self.labels['categorieFile'].config(text = self.selected['categorieFile'])
        
    def floderSelection(self):
        self.selected['saveDir'] = tkFileDialog.askdirectory(initialdir = "/", title = "Select export dir")
        self.labels['saveDir'].config(text = self.selected['saveDir'])
        
    def updateCategories(self):
        
        if not self.selected['table']:
            self.error('you must select a table')
            return 0 
        if not self.selected['categorieFile']:
            self.error('you must select a csv file')
            return 0 
        if self.selected['categorieFile'][-3:] != 'csv':
            self.error('you must select a csv file')
            return 0 
#        try:
#            table =self.selected['table']
#        except KeyError:
#            print 'you must select a table'
#            return 0
#        if type(table) != str:
#            print 'you must select a table'
#            return 0 
#        try:
#            categorieFile = self.selected['categorieFile']
#        except KeyError:
#            print 'you must select a csv file'
#            return 0
#        if categorieFile[-3:] != 'csv':
#            print 'you must select a csv file'
#            return 0
        SQL.categorize(dbName,self.selected['table'][0], self.selected['categorieFile'])
        
    def merge(self):
        try:
            Name = self.lists['MergeName'].get("1.0",'end')
        except KeyError:
            self.error( 'you must chose a name for merged data')
            return 0 
        try:
            Values = self.lists['toMergeNames'].get("1.0",'end')
        except KeyError:
            self.error( 'Select data to merge')
            return 0 
        Values = Values.split(',')
        try:
            table = self.selected['table']
        except KeyError:
            self.error( 'Select a table')
            return 0 
        try:
            column = self.selected['columns']
        except KeyError:
            print 'you must select a columns name'
            return 0 
        if self.Action.get() == 'Sum':
            action = 'SUM'
        elif self.Action.get() == 'Mean':
            action = 'AVG'
        print dbName,table,column,Name,Values,action
        if table == 'ALL':
            for tab in SQL.tables():
                tab=tab[0]
                SQL.merge(dbName,tab,column,Name,Values,action) 
        else:
            SQL.merge(dbName,table,column,Name,Values,action)
            
    def dropCol(self):
        if [] in [self.selected['table'], self.selected['columns']]:
            self.error( 'Select a table and a column')
        else:
            if self.selected['table'][0] == 'ALL':
                for table in SQL.tables():
                    table=table[0]
                    if len(self.selected['columns'])==1:
                        SQL.dropCol(table, self.selected['columns'][0])
                    else:
                        for col in self.selected['columns']:
                            SQL.dropCol(table, col)
            elif len(self.selected['columns'])==1:
                if len(self.selected['table'])==1:
                    SQL.dropCol(self.selected['table'][0], self.selected['columns'][0])
                else:
                    for table in self.selected['table']:
                        SQL.dropCol(table, self.selected['columns'][0])
            else:
                if len(self.selected['table'])==1:
                    for col in self.selected['columns']:
                        SQL.dropCol(self.selected['table'][0], col)
                else:
                    for table in self.selected['table']:
                        for col in self.selected['columns']:
                            SQL.dropCol(table, col)
                      
    def dropLines(self):
        features = ['Area', 'Year', 'Element', 'Item_categorie','Item','Unit']
        if [] in [self.selected['table'],self.selected['Area'],self.selected['Element'],self.selected['Item_categorie'],self.selected['Item'],self.selected['Unit']]:
            self.error( 'Not enough values selected')
        else:
            where = ''
            for feat in features:
                if self.selected[feat][0] != 'ALL':
                    if len(self.selected[feat])==1:
                        where += "and ({0} = '{1})' ".format(feat,self.selected[feat][0])
                    else:
                        where +="and ("+ SQL.orQuery(feat,self.selected[feat])+")"
            where = 'where ' + where[3:]
            if self.selected['table'][0] =='ALL':
                for table in SQL.tables():
                    table = table[0]
                    SQL.deleteQuery(table,where)
            elif len(self.selected['table'])==1:
                SQL.deleteQuery(self.selected['table'][0],where)
            else:
                for table in self.selected['table']:
                    SQL.deleteQuery(table,where)
#%% List functions             
    def resetSelection(self,feature):   
#        print self.labels
        if self.wincfg['window'] == 'Export':
            return 0
        if feature == 'ALL':
#            print feature
            for key in self.labels:
                if key != 'table':
                    self.selected[key] = []
                    self.objects[key] =[]
                    self.labels[key].config(text = 'curr:')
                    self.lists[key].delete(0,'end')
        else:
            self.selected[feature] = []
            self.objects[feature] =[]
            self.labels[feature].config(text = 'curr:')
            self.lists[feature].delete(0,'end')
        
    def fillListQuery(self,table,feature):    
        """
        ya moyen de réduire tt sa avec le dico
        
        """
        query = "select {0} from {1} group by {2}"       
        if self.wincfg['window'] == 'MAP':
            base = 'Year'
            order = ['Element','Item_categorie','Item']
        elif self.wincfg['window'] == 'BAR': 
            base = 'Area'
            order = ['Year','Element','Item_categorie','Item']
        elif self.wincfg['window'] == 'curves':
            base = 'Area'
            order = ['Element','Item_categorie','Item']
        elif self.wincfg['window']=='remove':
            base = 'Area'
            order = ['Year','Element','Item_categorie','Item','Unit']
        if feature == base:
            query = query.format(feature,table,feature)
            return query
        else:
            condition = order[:order.index(feature)]        
            cond = ''
            if self.selected[base][0] != 'ALL':
                cond = " where ({0} = {1}{2}{3}) "
                if base == 'Year':
                    if len(self.selected['Year']) == 1:
                        cond = cond.format('Year','',self.selected['Year'][0],'')
                    else:
                        cond = " where ("+SQL.orQuery('Year',self.selected['Year'])+")"
                else:
                    if len(self.selected['Area']) ==1:
                        cond = cond.format('Area',"'",self.selected['Area'][0],"'")
                    else:
                        cond = " where ("+SQL.orQuery('Area',self.selected['Area'])+')'
            if condition == []:
                query = query.format(feature, table + cond,feature)
                return query
            if type(condition) == str:
                if self.selected[condition][0] != 'ALL':
                    if cond != '':
                        if len(self.selected[condition])==1:
                            cond += " and ({0} = '{1}') ".format(condition,self.selected[condition][0])
                        else:
                            cond += " and ("+SQL.orQuery(condition,self.selected[condition])+')'
                    else:
                        if len(self.selected[condition])==1:
                            cond += " where ({0} = '{1}') ".format(condition,self.selected[condition][0])
                        else:
                            cond += " where ("+SQL.orQuery(condition,self.selected[condition])+")"
            else:
#                print cond
                for item in condition:
                    if self.selected[item][0] != 'ALL':
                        if len(self.selected[item]) ==1:
                            cond+= " and ({0} = '{1}') ".format(item,self.selected[item][0])
                        else:
                            cond += " and ("+SQL.orQuery(item,self.selected[item])+')'
#            print cond[:4]
            if cond[:4] == ' and':
                cond = " where " + cond[4:]
            query = query.format(feature, table + cond,feature)
            return query
            
    def fillList(self,table,featureArray):
        if self.wincfg['window'] == 'Export':
            return 0
            
        for feature in featureArray:
            if feature == 'Item' and self.selected['Item_categorie'][0] == 'ALL':
                things ={feature: ['SUM','MEAN']}
            else:
                query = self.fillListQuery(table,feature)
#                print query
                things = pd.read_sql(query,SQL.db)
            self.objects[feature] = things
            if self.wincfg['window'] in ['BAR','curves','remove']:
                self.lists[feature].insert('end', 'ALL')
            elif feature == 'Year':
                self.lists[feature].insert('end', 'ALL')
            for thing in things[feature] :
                self.lists[feature].insert('end', thing)

#    def onSelection(self,evnt,feature):
#        w = evnt.widget
#        index = int(w.curselection()[0])
#        value = w.get(index)
#        self.selected[feature] = value
#        self.labels[feature].config(text = 'curr : ' + str(value) ) 
#        if self.resetCfg[feature] != 'NULL':
#            if self.resetCfg[feature] != 'ALL':
#                for feat in self.resetCfg[feature]:
#                    self.resetSelection(feat)
#            else:
#                self.resetSelection('ALL')
#        if self.wincfg[feature]!='NULL':
#            self.fillList(self.selected['table'],self.wincfg[feature])
            
    def onSelection(self,evnt,feature):
        selection = list()
        w = evnt.widget
        index = w.curselection()
        for i in index:
            value = w.get(i)
            selection.append(value)
        self.selected[feature] = selection
        self.labels[feature].config(text = 'curr : ' + str(selection) ) 
        if self.resetCfg[feature] != 'NULL':
            if self.resetCfg[feature] != 'ALL':
                for feat in self.resetCfg[feature]:
                    self.resetSelection(feat)
            else:
                self.resetSelection('ALL')
        if self.wincfg[feature]!='NULL':
            self.fillList(self.selected['table'][0],self.wincfg[feature])
            
    def onCheck(self,database):
        SQL.selectDb(database)
        self.lists['table'].delete(1,'end')
        tables = SQL.tables()
        tables = [x[0] for x in tables]
        for table in tables:
            self.lists['table'].insert('end',table)
        SQL.selectDb(dbName)
#%% windows setting
    def initialize(self,window):
        self.wincfg = {}
        self.resetCfg = {}
        def listCreator(self,feature, position = None,widg=None,multiple = False):
            listname = 'list'+feature
            self.widgs[listname] = Tkinter.PanedWindow(self, orient='vertical')
            self.widgs[listname].pack(side='top')
            label = Tkinter.Label(self, text=feature)
            self.infoLabel.append(label)
            self.widgs[listname].add( label)
            if not multiple:
                self.lists[feature] = Tkinter.Listbox(self)
                self.lists[feature].bind('<<ListboxSelect>>',lambda event: self.onSelection(event ,feature))
            else:
                self.lists[feature] = Tkinter.Listbox(self,selectmode='multiple')
#                self.lists[feature].bind('<<ListboxSelect>>',lambda event: self.onMultiSelection(event ,feature))
            self.lists[feature].bind('<<ListboxSelect>>',lambda event: self.onSelection(event ,feature))
            self.lists[feature].pack()
            self.widgs[listname].add(self.lists[feature])
            self.labels[feature]=Tkinter.Label(self, text="curr:")
            self.widgs[listname].add(self.labels[feature])
            label = Tkinter.Label(self, text="")
            self.widgs[listname].add(label )
            self.infoLabel.append(label)
#            print feature
#            if feature == 'columns':
#                    print 54545454654651651984
            if feature  in ['columns','table']:
                if feature == 'columns':
                    things = SQL.columns(SQL.tables()[0][0])
                if feature == 'table':
                    if self.wincfg['window'] in ['Export','merge','remove']:
                        self.lists[feature].insert('end', 'ALL')
                    things = [x[0] for x in  SQL.tables()]
                for thing in things:
                    self.lists[feature].insert('end', thing)
            
            self.widgs[listname].pack()    
            if position != None:
                self.widgs[listname].grid(column=position[0],row=position[1])
            elif widg != None:
                widg.add(self.widgs[listname])
            
        def buttonCreator(self,name,action,position=None,widg=None): 
            button = Tkinter.Button(self,text=name ,command = action)
            if position!=None:
                button.grid(column=position[0],row=position[1])
            elif widg!=None:
                widg.add(button)
            self.button.append(button)
            
        def radioButton(self,name,variable,value,position=None,widg=None,command=None):
            button = Tkinter.Radiobutton(self, text=name, variable=variable, value=value,command = command)
            if position != None:
                button.grid(column = position[0],row=position[1])
                self.button.append(button)
            elif widg!=None:
                widg.add(button)
 
        def title(self,title):
            label = Tkinter.Label(self, text=title,bd=5)
            label.grid(column=0,row=0)
            self.infoLabel.append(label)
            
        if window == 'MAP':
            self.wincfg={'table':['Year'], 'Year':['Element'],'Element':['Item_categorie'],
                         'Item_categorie':['Item'],'Item':'NULL'}
            
            self.resetCfg = {'table':['Year','Element','Item_categorie','Item'], 
                         'Year':['Element','Item_categorie','Item'], 
                         'Element':['Item_categorie','Item'],
                         'Item_categorie' :['Item'],
                         'Item':'NULL','Unit':'NULL' }
            self.wincfg['window']='MAP'
            
            #window title
            title(self, 'MAP visualisation')
            
            #List and button
            self.widgs['globallist'] = Tkinter.PanedWindow(self, orient='horizontal')
            self.widgs['globallist'].pack(side='top')
            #  Collections liste
            listCreator(self,'table',widg =self.widgs['globallist'] )  
            # Year List
            listCreator(self,'Year',widg =self.widgs['globallist']) 
            # item list
            listCreator(self,'Element',widg =self.widgs['globallist']) 
            #ItemCategorie List
            listCreator(self,'Item_categorie',widg =self.widgs['globallist'])
            # element list
            listCreator(self,'Item',widg =self.widgs['globallist'])
            
            self.widgs['globallist'].pack(side='top')
            # Draw Button   
            buttonCreator(self,'draw',self.drawMap,widg =self.widgs['globallist'])
            
            # Display All list 
            self.widgs['globallist'].grid(column = 0,row = 1)
            
            #Radio button     
            self.widgs['globalradio'] = Tkinter.PanedWindow(self, orient='horizontal')
            self.widgs['globalradio'].add(Tkinter.Label(self, text="Ponderation:"))

            self.ponderate = Tkinter.IntVar(self)
            radioButton(self, "None", self.ponderate, 0, widg=self.widgs['globalradio'] )
            radioButton(self, "Population", self.ponderate, 1, widg=self.widgs['globalradio'])
            radioButton(self, "Agricultural area", self.ponderate, 2, widg=self.widgs['globalradio'])
            self.ponderate.set(0)           
            self.widgs['globalradio'].grid(column = 0,row = 2)
            
        elif window == 'BAR':
            self.wincfg={'table':['Area'],'Area':['Year'], 'Year':['Element'],'Element':['Item_categorie'],
                         'Item_categorie':['Item'],'Item':'NULL'}
            
            self.resetCfg = {'table':['Area','Year','Element','Item_categorie','Item'], 
                             'Area' :['Year','Element','Item_categorie','Item'],
                             'Year':['Element','Item_categorie','Item'],
                             'Element':['Item_categorie','Item'],
                             'Item_categorie' :['Item'],
                             'Item':'NULL','Unit':'NULL'}
            self.wincfg['window']='BAR'
            
            #window title
            title(self, 'BarGraph')
            
            #List and button
            self.widgs['globallist'] = Tkinter.PanedWindow(self, orient='horizontal')
            self.widgs['globallist'].pack(side='top')
            #  Collections liste
            listCreator(self,'table',widg =self.widgs['globallist']) 
            # AreaList
            listCreator(self,'Area',widg =self.widgs['globallist']) 
            # Year List
            listCreator(self,'Year',widg =self.widgs['globallist']) 
            # item list
            listCreator(self,'Element',widg =self.widgs['globallist']) 
            #ItemCategorie List
            listCreator(self,'Item_categorie',widg =self.widgs['globallist']) 
            # element list
            listCreator(self,'Item',widg =self.widgs['globallist'])  
            # Draw Button   
            buttonCreator(self,'draw',self.drawBar,widg =self.widgs['globallist'])
            # Display All list 
            self.widgs['globallist'].grid(column = 0,row = 1)
            
        elif window == 'curves':
            self.wincfg={'table':['Area'],'Area':['Element'],'Element':['Item_categorie'],
                         'Item_categorie':['Item'],'Item':'NULL'}
            
            self.resetCfg = {'table':['Area','Element','Item_categorie','Item'], 
                             'Area' :['Element','Item_categorie','Item'],
                             'Element':['Item_categorie','Item'],
                             'Item_categorie' :['Item'],
                             'Item':'NULL','Unit':'NULL' }
            self.wincfg['window']='curves'
            
            #window title
            title(self, 'time series visualisation')
            
            #List and button
            self.widgs['globallist'] = Tkinter.PanedWindow(self, orient='horizontal')
            self.widgs['globallist'].pack(side='top')
            #  Collections liste
            listCreator(self,'table',widg =self.widgs['globallist']) 
            # AreaList
            listCreator(self,'Area',widg =self.widgs['globallist'],multiple=True) 
            # element list
            listCreator(self,'Element',widg =self.widgs['globallist'],multiple=True) 
            #ItemCategorie List
            listCreator(self,'Item_categorie',widg =self.widgs['globallist'],multiple=True) 
            # item list
            listCreator(self,'Item',widg =self.widgs['globallist'],multiple=True)
            # Draw Button   
            buttonCreator(self,'draw', self.drawCurves ,widg =self.widgs['globallist'])
            # Display All list 
            self.widgs['globallist'].grid(column = 0,row = 1)
            
        elif window == 'Export':
            self.wincfg['window']='Export'
            self.wincfg['table']='NULL'
            self.resetCfg['table']='NULL'
            self.selected['saveDir']=rootDir
            
            #window title
            title(self, 'Export database')
            # Database selection
            self.widgs['db'] = Tkinter.PanedWindow(self, orient='vertical')
            self.widgs['db'].pack(side='top')
            label = Tkinter.Label(self, text='Database')
            self.widgs['db'].add(label)
            self.infoLabel.append(label)
            self.db = Tkinter.StringVar(self)
            radioButton(self, dbName, self.db, 'fao', widg=self.widgs['db'],command = lambda :self.onCheck(dbName))
            radioButton(self, "reshaped", self.db, 'reshaped', widg=self.widgs['db'],command = lambda :self.onCheck('reshaped'))
            self.widgs['db'].grid(column=0, row=1)
            self.db.set('fao')
            
            
            #  Tables liste
            listCreator(self,'table',position=(1,1))
            #Area Selection
            self.widgs['area'] = Tkinter.PanedWindow(self, orient='vertical')
            self.widgs['area'].pack(side='top')
            label = Tkinter.Label(self, text='Areas')
            self.widgs['area'].add( label)
            self.infoLabel.append(label)
            self.area = Tkinter.StringVar(self)
            radioButton(self, 'country', self.area, 'country', widg=self.widgs['area'])
            radioButton(self, "continent", self.area, 'continent', widg=self.widgs['area'])
            radioButton(self, "ALL", self.area, 'ALL', widg=self.widgs['area'])
            self.widgs['area'].grid(column=2, row=1)
            self.area.set('country')
            #Item Selection
            self.widgs['item'] = Tkinter.PanedWindow(self, orient='vertical')
            self.widgs['item'].pack(side='top')
            label = Tkinter.Label(self, text='Items') 
            self.widgs['item'].add(label)
            self.infoLabel.append(label)
            self.item = Tkinter.StringVar(self)
            radioButton(self, 'basic', self.item, 'basic', widg=self.widgs['item'])
            radioButton(self, "FAOtotal", self.item, 'total', widg=self.widgs['item'])
            radioButton(self, "ALL", self.item, 'ALL', widg=self.widgs['item'])
            self.widgs['item'].grid(column=3, row=1)
            self.item.set('basic')       
            #clean Selection
            self.widgs['clean'] = Tkinter.PanedWindow(self, orient='vertical')
            self.widgs['clean'].pack(side='top')
            label = Tkinter.Label(self, text='Keep country in all tables only') 
            self.widgs['clean'].add(label)
            self.infoLabel.append(label)
            self.clean = Tkinter.StringVar(self)
            radioButton(self, 'no', self.clean, 'None', widg=self.widgs['clean'])
            radioButton(self, "yes", self.clean, 'learning', widg=self.widgs['clean'])
            self.widgs['clean'].grid(column=4, row=1)
            self.clean.set('None') 
            # Export Button   
            buttonCreator(self,'Export',self.export,position=(5,1))
            #directory to save
            # Select categorie File to apply
            self.widgs['saveDir'] = Tkinter.PanedWindow(self, orient='vertical')
            self.labels['saveDir'] = Tkinter.Label(self,text = rootDir)
            self.widgs['saveDir'].add(self.labels['saveDir'])
            # Categorie file selection button
            buttonCreator(self,'Save dir', self.floderSelection ,widg=self.widgs['saveDir'])
            self.widgs['saveDir'].grid(column = 0,row = 2)
            
        elif window == 'loading':
            self.wincfg['window']='loading'
            #window title
            title(self, 'Load fao csv into a SQL database')
            # Load data button
            buttonCreator(self,'load data', self.dataLoad ,position=(0,1))
            
        elif window == 'fullProcess':
            self.wincfg['window']='fullProcess'
            #window title
            title(self, 'Run full process and reshaps all tables')
            # full Process button
            buttonCreator(self,'Full process', self.fullProcess ,position=(0,1))
            
        elif window == 'categorise':
            self.wincfg['window']='categorise'
            #window title
            title(self, 'Use catégorie file on a table')
            # Select categorie File to apply
            self.widgs['categorieFile'] = Tkinter.PanedWindow(self, orient='vertical')
            self.labels['categorieFile'] = Tkinter.Label(self,text = 'No File Selected               ')
            self.widgs['categorieFile'].add(self.labels['categorieFile'])
            # Categorie file selection button
            buttonCreator(self,'Categorie file', self.fileSelection ,widg=self.widgs['categorieFile'])
            self.widgs['categorieFile'].grid(column = 0,row = 1)
            
            #  Tables liste
            listCreator(self,'table',position=(1,1))
            # Categorise button
            buttonCreator(self,'Update Categorie', self.updateCategories ,position=(2,1))
            
        elif window == 'remove':
            self.wincfg={'table':['Area'],'Area':['Year'], 'Year':['Element'],'Element':['Item_categorie'],
                         'Item_categorie':['Item'],'Item':['Unit'],'Unit':'NULL','columns':'NULL'}
            
            self.resetCfg = {'table':['Area','Year','Element','Item_categorie','Item'], 
                             'Area' :['Year','Element','Item_categorie','Item'],
                             'Year':['Element','Item_categorie','Item'],
                             'Element':['Item_categorie','Item'],
                             'Item_categorie' :['Item','Unit'],
                             'Item':['Unit'],'Unit':'NULL'  ,
                             'columns':'NULL'}
            
            self.wincfg['window']='remove'
            #window title
            title(self, 'Remove Somme datas')
            
            """ Remove columns """
            self.widgs['removecols'] = Tkinter.PanedWindow(self, orient='vertical')
            label = Tkinter.Label(self, text='Remove somme data')
            self.widgs['removecols'].add(label)
            self.infoLabel.append(label)
            # table selection
            listCreator(self,'table',position=(0,1),multiple=True)
            #column selection
            listCreator(self,'columns',position=(1,1),multiple=True)
            # remove button
            buttonCreator(self,'Remove column',  self.dropCol ,position=(2,1))
            
            """remove Lines """
            # table selection
#            listCreator(self,'table',position=(0,1))
            # Area selection ( can be all)
            listCreator(self,'Area',position=(1,2),multiple=True)
            # year selection ( can be all)
            listCreator(self,'Year',position=(2,2),multiple=True)
            # element selection ( can be all)
            listCreator(self,'Element',position=(3,2),multiple=True)
            # item_categorie selection ( can be all)
            listCreator(self,'Item_categorie',position=(4,2),multiple=True)
            #item
            listCreator(self,'Item',position=(5,2),multiple=True)
            #Unit
            listCreator(self,'Unit',position=(6,2),multiple=True)
            #remove button
            buttonCreator(self,'Remove line(s)',  self.dropLines ,position=(7,2))
#            
        elif window == 'reshape':
            self.wincfg['window']='reshape'
            #window title
            title(self, 'Reshape the database')
            
            # update button
            buttonCreator(self,'Reshape database',  self.reshapedb ,position=(0,1))
            
        elif window == 'merge':
            self.wincfg={'table':'NULL', 'columns' :'NULL'}
            
            self.resetCfg = {'table':'NULL', 
                             'columns' :'NULL' }
            
            self.wincfg['window']='merge'
            #window title
            title(self, 'Sum or Mean Values of multiple countrys')#or item or elem...
            # Table Selection
            listCreator(self,'table',position =(0,1))
            # collumn to Merge selection
            listCreator(self,'columns',position =(1,1))
            # Get a list of element to merge,separated by comas
            self.widgs['Values'] = Tkinter.PanedWindow(self, orient='vertical')
            label = Tkinter.Label(self, text='List of Values to merge,separated by comas, for exemple : \n  France, Belgium')
            self.infoLabel.append(label)
            self.widgs['Values'].add(label)
            toMergeNames = Tkinter.Text(self,height=10,width=20)
            self.lists['toMergeNames']=toMergeNames
            self.widgs['Values'].add(toMergeNames)
            self.widgs['Values'].grid(column = 2 , row = 1)
            # Action Mean or Sum Values
            self.widgs['Action'] = Tkinter.PanedWindow(self, orient='vertical')
            label = Tkinter.Label(self, text='Action')
            self.infoLabel.append(label)
            self.widgs['Action'].add(label)
            self.Action = Tkinter.StringVar(self)
            radioButton(self, 'Sum', self.Action, 'Sum', widg=self.widgs['Action'])
            radioButton(self, 'Mean', self.Action, 'Mean', widg=self.widgs['Action'])
            self.widgs['Action'].grid(column = 3 , row = 1)
            self.Action.set('Sum') 
            #Merge name selection
            self.widgs['Name'] = Tkinter.PanedWindow(self, orient='vertical')
            label = Tkinter.Label(self, text='Merge Name')
            self.infoLabel.append(label)
            self.widgs['Name'].add(label)
            MergeName = Tkinter.Text(self,height=2,width=10)
            self.lists['MergeName']=MergeName
            self.widgs['Name'].add(MergeName)
            # merge button
            buttonCreator(self,'Sum Item',  self.merge ,widg=self.widgs['Name'])
            self.widgs['Name'].grid(column =4 , row =1 )
                      
        elif window == 'SQL':
            self.wincfg['window']='SQL'
            #window title
            title(self, 'Execute a SQL querry')
            # query zonne
            query = Tkinter.Text(self,height=10)
            query.grid(column = 0,row=1)
            self.lists['query']=query
            #return zonne
            label = Tkinter.Label(self, text='out: ')
            self.infoLabel.append(label)
            label.grid(column = 0,row=2)
            ret = Tkinter.Text(self,height=10)
            ret.bind("<Key>", lambda e: "break")
            ret.grid(column = 0,row=3)
            self.lists['return']=ret
            # run command button
            buttonCreator(self,'run',  self.runQuery ,position=(1,1))
#%% Main
"""main"""
if __name__ == "__main__":
    # open configFile
    try:
        configfile = pd.read_csv( 'configFiles/config.csv' , sep=',', header=0)
    except IOError:
        """No config file detected"""
       
    # Path for cleaning fils
    cleanPath = 'configFiles/cleaningFiles/'
    itemsClean = cleanPath + 'itemsClean.csv'
    countrysClean = cleanPath + 'countrysClean.csv'
    # Path for categories files
    itemCatPath = 'configFiles/categorieFiles/'

    # connect database
    dbName = configfile.dbName[0]
    dbHost = str(configfile.dbHost[0])
    dbUser = configfile.dbUser[0]
    Passwd = configfile.Passwd[0]

    limiteYear = configfile.limiteYear[0]
    SQL = SQLtools.SQLCo(dbHost,dbUser,Passwd)
    if (dbName,) not in SQL.databases():
        SQL.createDb(dbName)
    SQL.selectDb(dbName)
    # get tables list
    tables = SQL.tables()
    # start app
    app = WorldMapWindow(None)
    app.title('FAO tools')
    app.mainloop()